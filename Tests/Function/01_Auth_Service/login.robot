*** Settings ***
Library         SeleniumLibrary
Resource        ../../../Resources/commons.resource
Resource        ../../../Resources/01_Auth/login.resource        

Suite Setup     Configurate Selenium


*** Test Cases ***
TC01 - Login Email Successfully
    [Setup]    Navigate To Home Page
    Check Valid Email
    Verify On Home Page
    [Teardown]    Exit Selenium

TC02 - Login Invalid Email
    [Setup]    Navigate To Home Page
    Check Invalid Email
    Noti Login Email Unsuccessful
    [Teardown]    Exit Selenium

TC03 - Login Phone Number Successfully
    [Setup]    Navigate To Home Page
    Switch To Phone Number
    Check Valid Phone Number
    Verify On Home Page
    [Teardown]    Exit Selenium

TC04 - Login Invalid Phone Number Successfully
    [Setup]    Navigate To Home Page
    Switch To Phone Number
    Check Invalid Phone Number
    Noti Login Phone Number Unsuccessful
    [Teardown]    Exit Selenium
